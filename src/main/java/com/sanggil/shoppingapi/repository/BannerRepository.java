package com.sanggil.shoppingapi.repository;

import com.sanggil.shoppingapi.entity.Banner;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BannerRepository extends JpaRepository<Banner, Long> {
    List<Banner> findAllByIsUseOrderByIdAsc(Boolean isUse);
}
