package com.sanggil.shoppingapi.repository;

import com.sanggil.shoppingapi.entity.Goods;
import com.sanggil.shoppingapi.enums.GoodsCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GoodsRepository extends JpaRepository<Goods, Long> {
    List<Goods> findAllByGoodsCategoryOrderByGoodsNameAsc(GoodsCategory goodsCategory);
}
