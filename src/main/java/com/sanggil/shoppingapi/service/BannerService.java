package com.sanggil.shoppingapi.service;

import com.sanggil.shoppingapi.entity.Banner;
import com.sanggil.shoppingapi.exception.CMissingDataException;
import com.sanggil.shoppingapi.model.BannerItem;
import com.sanggil.shoppingapi.model.BannerRequest;
import com.sanggil.shoppingapi.model.BannerUpdateRequest;
import com.sanggil.shoppingapi.model.ListResult;
import com.sanggil.shoppingapi.repository.BannerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BannerService {
    private final BannerRepository bannerRepository;

    public void setBanner(BannerRequest request) {
        Banner addData = new Banner.BannerBuilder(request).build();
        bannerRepository.save(addData);
    }

    public ListResult<BannerItem> getBanner() {
        List<Banner> originList = bannerRepository.findAllByIsUseOrderByIdAsc(true);

        List<BannerItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new BannerItem.BannerItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public void putBanner(long id, BannerUpdateRequest updateRequest) {
        Banner originData = bannerRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putBanner(updateRequest);

        bannerRepository.save(originData);
    }

    public void delBanner(long id) {
        bannerRepository.deleteById(id);
    }
}
