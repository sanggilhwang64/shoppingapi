package com.sanggil.shoppingapi.service;

import com.sanggil.shoppingapi.entity.Member;
import com.sanggil.shoppingapi.exception.CNoIdPassword;
import com.sanggil.shoppingapi.model.LoginRequest;
import com.sanggil.shoppingapi.model.LoginResponse;
import com.sanggil.shoppingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public LoginResponse doLogin(LoginRequest loginRequest) {
        Member member = memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CNoIdPassword::new);

        if (!member.getPassword().equals(loginRequest.getPassword())) throw new CNoIdPassword();

        return new LoginResponse.Builder(member).build();
    }
}
