package com.sanggil.shoppingapi.service;

import com.sanggil.shoppingapi.entity.Goods;
import com.sanggil.shoppingapi.enums.GoodsCategory;
import com.sanggil.shoppingapi.model.BannerItem;
import com.sanggil.shoppingapi.model.GoodsItem;
import com.sanggil.shoppingapi.model.GoodsRequest;
import com.sanggil.shoppingapi.model.ListResult;
import com.sanggil.shoppingapi.repository.GoodsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GoodsService {
    private final GoodsRepository goodsRepository;

    public void setGoods(GoodsRequest request) {
        Goods addData = new Goods.GoodsBuilder(request).build();
        goodsRepository.save(addData);
    }

    public ListResult<GoodsItem> getGoodsByCategory(GoodsCategory goodsCategory) {
        List<Goods> originList = goodsRepository.findAllByGoodsCategoryOrderByGoodsNameAsc(goodsCategory);

        List<GoodsItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new GoodsItem.GoodsItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
}
