package com.sanggil.shoppingapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BannerRequest {
    @ApiModelProperty(notes = "배너 이미지 주소")
    @NotNull
    @Length(min = 2, max = 200)
    private String bannerImageUrl;

    @ApiModelProperty(notes = "사용여부")
    @NotNull
    private Boolean isUse;
}
