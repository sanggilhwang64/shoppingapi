package com.sanggil.shoppingapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BannerUpdateRequest {
    @ApiModelProperty(notes = "사용여부")
    @NotNull
    private Boolean isUse;
}
