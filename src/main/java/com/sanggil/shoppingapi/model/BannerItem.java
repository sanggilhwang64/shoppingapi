package com.sanggil.shoppingapi.model;

import com.sanggil.shoppingapi.entity.Banner;
import com.sanggil.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BannerItem {
    @ApiModelProperty(notes = "배너 이미지 주소")
    private String bannerImageUrl;

    @ApiModelProperty(notes = "사용여부")
    private Boolean isUse;

    private BannerItem(BannerItemBuilder builder) {
        this.bannerImageUrl = builder.bannerImageUrl;
        this.isUse = builder.isUse;
    }

    public static class BannerItemBuilder implements CommonModelBuilder<BannerItem> {
        private final String bannerImageUrl;
        private final Boolean isUse;

        public BannerItemBuilder(Banner banner) {
            this.bannerImageUrl = banner.getBannerImageUrl();
            this.isUse = banner.getIsUse();
        }

        @Override
        public BannerItem build() {
            return new BannerItem(this);
        }
    }
}
