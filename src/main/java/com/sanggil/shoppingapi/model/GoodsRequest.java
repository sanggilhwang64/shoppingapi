package com.sanggil.shoppingapi.model;

import com.sanggil.shoppingapi.enums.GoodsCategory;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GoodsRequest {
    @ApiModelProperty(notes = "카테고리")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private GoodsCategory goodsCategory;

    @ApiModelProperty(notes = "상품 이미지 주소")
    @NotNull
    @Length(min = 2, max = 200)
    private String goodsImageUrl;

    @NotNull
    @Length(min = 2, max = 50)
    @ApiModelProperty(notes = "상품 이름")
    private String goodsName;

    @ApiModelProperty(notes = "원가")
    @NotNull
    private Double costPrice;

    @ApiModelProperty(notes = "할인 가격")
    @NotNull
    private Double salePrice;

    @ApiModelProperty(notes = "총 가격")
    @NotNull
    private Double totalPrice;
}
