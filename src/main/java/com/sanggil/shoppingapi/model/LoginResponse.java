package com.sanggil.shoppingapi.model;

import com.sanggil.shoppingapi.entity.Member;
import com.sanggil.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "고객명")
    private String name;

    private LoginResponse(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
    }

    public static class Builder implements CommonModelBuilder<LoginResponse> {
        private final Long id;
        private final String name;

        public Builder(Member member) {
            this.id = member.getId();
            this.name = member.getName();
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
