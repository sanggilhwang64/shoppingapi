package com.sanggil.shoppingapi.model;

import com.sanggil.shoppingapi.entity.Goods;
import com.sanggil.shoppingapi.enums.GoodsCategory;
import com.sanggil.shoppingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GoodsItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "카테고리")
    @Enumerated(EnumType.STRING)
    private String goodsCategory;

    @ApiModelProperty(notes = "상품 이미지 주소")
    private String goodsImageUrl;

    @ApiModelProperty(notes = "상품 이름")
    private String goodsName;

    @ApiModelProperty(notes = "원가")
    private Double costPrice;

    @ApiModelProperty(notes = "할인 가격")
    private Double salePrice;

    @ApiModelProperty(notes = "총 가격")
    private Double totalPrice;

    private GoodsItem(GoodsItemBuilder builder) {
        this.id = builder.id;
        this.goodsCategory = builder.goodsCategory;
        this.goodsImageUrl = builder.goodsImageUrl;
        this.goodsName = builder.goodsName;
        this.costPrice = builder.costPrice;
        this.salePrice = builder.salePrice;
        this.totalPrice = builder.totalPrice;
    }

    public static class GoodsItemBuilder implements CommonModelBuilder<GoodsItem> {
        private final Long id;
        private final String goodsCategory;
        private final String goodsImageUrl;
        private final String goodsName;
        private final Double costPrice;
        private final Double salePrice;
        private final Double totalPrice;

        public GoodsItemBuilder(Goods goods) {
            this.id = goods.getId();
            this.goodsCategory = goods.getGoodsCategory().getCategoryName();
            this.goodsImageUrl = goods.getGoodsImageUrl();
            this.goodsName = goods.getGoodsName();
            this.costPrice = goods.getCostPrice();
            this.salePrice = goods.getSalePrice();
            this.totalPrice = goods.getTotalPrice();
        }

        @Override
        public GoodsItem build() {
            return new GoodsItem(this);
        }
    }
}
