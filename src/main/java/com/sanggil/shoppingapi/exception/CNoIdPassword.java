package com.sanggil.shoppingapi.exception;

public class CNoIdPassword extends RuntimeException {
    public CNoIdPassword(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoIdPassword(String msg) {
        super(msg);
    }

    public CNoIdPassword() {
        super();
    }
}
