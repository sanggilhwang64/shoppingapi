package com.sanggil.shoppingapi.entity;

import com.sanggil.shoppingapi.interfaces.CommonModelBuilder;
import com.sanggil.shoppingapi.model.BannerRequest;
import com.sanggil.shoppingapi.model.BannerUpdateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Banner {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "배너 이미지 주소")
    @Column(nullable = false, length = 200)
    private String bannerImageUrl;

    @ApiModelProperty(notes = "사용여부")
    @Column(nullable = false)
    private Boolean isUse;

    public void putBanner(BannerUpdateRequest updateRequest) {
        this.isUse = updateRequest.getIsUse();
    }

    private Banner(BannerBuilder builder) {
        this.bannerImageUrl = builder.bannerImageUrl;
        this.isUse = builder.isUse;
    }

    public static class BannerBuilder implements CommonModelBuilder<Banner> {
        private final String bannerImageUrl;
        private final Boolean isUse;

        public BannerBuilder(BannerRequest request) {
            this.bannerImageUrl = request.getBannerImageUrl();
            this.isUse = request.getIsUse();
        }

        @Override
        public Banner build() {
            return new Banner(this);
        }
    }
}
