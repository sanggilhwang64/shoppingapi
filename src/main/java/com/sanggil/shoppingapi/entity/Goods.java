package com.sanggil.shoppingapi.entity;

import com.sanggil.shoppingapi.enums.GoodsCategory;
import com.sanggil.shoppingapi.interfaces.CommonModelBuilder;
import com.sanggil.shoppingapi.model.GoodsRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Goods {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "카테고리")
    @Column(nullable = false, length = 50)
    @Enumerated(value = EnumType.STRING)
    private GoodsCategory goodsCategory;

    @ApiModelProperty(notes = "상품 이미지 주소")
    @Column(nullable = false, length = 200)
    private String goodsImageUrl;

    @ApiModelProperty(notes = "상품 이름")
    @Column(nullable = false, length = 50)
    private String goodsName;

    @ApiModelProperty(notes = "원가")
    @Column(nullable = false)
    private Double costPrice;

    @ApiModelProperty(notes = "할인 가격")
    @Column(nullable = false)
    private Double salePrice;

    @ApiModelProperty(notes = "총 가격")
    @Column(nullable = false)
    private Double totalPrice;

    private Goods(GoodsBuilder builder) {
        this.goodsCategory = builder.goodsCategory;
        this.goodsImageUrl = builder.goodsImageUrl;
        this.goodsName = builder.goodsName;
        this.costPrice = builder.costPrice;
        this.salePrice = builder.salePrice;
        this.totalPrice = builder.totalPrice;
    }

    public static class GoodsBuilder implements CommonModelBuilder<Goods> {
        private final GoodsCategory goodsCategory;
        private final String goodsImageUrl;
        private final String goodsName;
        private final Double costPrice;
        private final Double salePrice;
        private final Double totalPrice;

        public GoodsBuilder(GoodsRequest request) {
            this.goodsCategory = request.getGoodsCategory();
            this.goodsImageUrl = request.getGoodsImageUrl();
            this.goodsName = request.getGoodsName();
            this.costPrice = request.getCostPrice();
            this.salePrice = request.getSalePrice();
            this.totalPrice = request.getTotalPrice();

        }
        @Override
        public Goods build() {
            return new Goods(this);
        }
    }
}
