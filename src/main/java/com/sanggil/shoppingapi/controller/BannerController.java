package com.sanggil.shoppingapi.controller;

import com.sanggil.shoppingapi.model.*;
import com.sanggil.shoppingapi.service.BannerService;
import com.sanggil.shoppingapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "배너")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/banner")
public class BannerController {
    private final BannerService bannerService;

    @ApiOperation(value = "배너 등록")
    @PostMapping("/new")
    public CommonResult setBanner(@RequestBody @Valid BannerRequest request) {
        bannerService.setBanner(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "배너 리스트")
    @GetMapping("/all")
    public ListResult<BannerItem> getBanner() {
        return ResponseService.getListResult(bannerService.getBanner(), true);
    }

    @ApiOperation(value = "배너 수정")
    @PutMapping("/update/{id}")
    public CommonResult putBanner(@PathVariable long id, @RequestBody @Valid BannerUpdateRequest updateRequest) {
        bannerService.putBanner(id, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "배너 삭제")
    @DeleteMapping("/del/{id}")
    public CommonResult delBanner(@PathVariable long id) {
        bannerService.delBanner(id);

        return ResponseService.getSuccessResult();
    }
}
