package com.sanggil.shoppingapi.controller;

import com.sanggil.shoppingapi.entity.Goods;
import com.sanggil.shoppingapi.enums.GoodsCategory;
import com.sanggil.shoppingapi.model.CommonResult;
import com.sanggil.shoppingapi.model.GoodsItem;
import com.sanggil.shoppingapi.model.GoodsRequest;
import com.sanggil.shoppingapi.model.ListResult;
import com.sanggil.shoppingapi.service.GoodsService;
import com.sanggil.shoppingapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "상품")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/goods")
public class GoodsController {
    private final GoodsService goodsService;

    @ApiOperation(value = "상품 등록")
    @PostMapping("/new")
    public CommonResult setGoods(@RequestBody @Valid GoodsRequest request) {
        goodsService.setGoods(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "카테고리 상품 리스트")
    @GetMapping("/all/category")
    public ListResult<GoodsItem> getGoods(@RequestParam GoodsCategory goodsCategory) {
        return ResponseService.getListResult(goodsService.getGoodsByCategory(goodsCategory), true);
    }

}
