package com.sanggil.shoppingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GoodsCategory {
    FOOD("식품");

    private final String categoryName;
}
