package com.sanggil.shoppingapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
